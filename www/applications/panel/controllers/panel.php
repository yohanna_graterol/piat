<?php
/**
 * Access from index.php:
 */
class panel_Controller extends ZP_Controller {
	
	public function __construct() {
		$this->app("panel");
		$this->Files = $this->core("Files");
		$this->Templates = $this->core("Templates");
		$this->Templates->theme("backend");
		$this->helper("alerts");
		$this->helper("sessions");
		$this->helper("security");
		$this->panel_Model = $this->model("panel_Model");
	}

	public function index(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="panelrio")){
			$this->home();
		}else{
			$this->login(); 
		}
	}
	public function registrar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("registrar")){
				$clave = encrypt(POST("clave"),1,TRUE,TRUE);
				$data = $this->panel_Model->registrar($clave);
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->home();
			}else{
				$this->title("Registro Profesor");
				$vars["tipo"] = $_GET["tipo"];
				$vars["view"] = $this->view("registrar",TRUE);
				$this->render("content",$vars);
			}
		}else{}
	}
	//Login es la funcion encargada de manipular el acceso al sistema
	public function login(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$this->home();
		}else{
			if(POST("entrar")){
				$clave = encrypt(POST("clave"),1,TRUE,TRUE);
				$data = $this->panel_Model->login($clave);
				//____($data);
				if($data == false){
					$this->title("Error al Entrar");
					$vars["alert"] = "alerta";
					echo "<script>alert('Usuario o Clave Incorrecto')</script>";
					$vars["view"] = $this->view("login",TRUE);
					$this->render("content",$vars);
				}else{
					if($data[0]["estatus"]=="Activado"){


						SESSION("tipo_user", $data[0]["tipo_user"]);
						SESSION("nombre", $data[0]["nombres"]);
						SESSION("apellido", $data[0]["apellidos"]);
						SESSION("id", $data[0]["id"]);
						SESSION("cedula", $data[0]["cedula"]);
						SESSION("direccion", $data[0]["direccion"]);
						SESSION("email", $data[0]["correo"]);
						SESSION("telefono", $data[0]["telefono"]);
						//$logs = $this->panel_Model->logs("Entrar");
											
						$this->home();
					}else{
						echo "<script>alert('Su Cuenta esta Bloqueada Contacte con el Administrador del Sistema');</script>";
						echo "<script>document.location.href=''</script>";
					}
				}
			}else{
				$this->title("Entrar al Sistema");
				$vars["view"] = $this->view("login",TRUE);
				$this->render("content",$vars);
			}
		}
	}
	
	public function home() {	
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="opei")){
			
			$this->title("Todas los Profesores");
			$vars["profesores"] = $this->panel_Model->buscar_todos_usuarios();
			$vars["view"] = $this->view("home",TRUE);
			$this->render("content",$vars);
				
			//$logs = $this->panel_Model->logs("Ver Libros Prestados");
			
		}else{
			$this->index(); 
		}
	}
	public function salir() {	
		$logs = $this->panel_Model->logs("Salir");		
 		unsetSessions($URL);
		$this->index(); 
	}
	public function perfil_usuario(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="panelrio")){
			if(POST("modificar")){
				$uploaddir = '/opt/lampp/htdocs/www/lib/themes/panel/img/';
				$uploadfile = $uploaddir.basename($_FILES['userfile']['name']);
				$namefile = basename($_FILES['userfile']['name']);

				if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
					chmod($uploadfile, 0755);
					$data = $this->panel_Model->modificar_perfil(SESSION("id"),$namefile);
				
					echo "<script>alert('Datos Guardados con Exito Vuelva a Entrar al Sistema');</script>";
					$logs = $this->panel_Model->logs("Modificar Perfil");
					$this->salir();
				} else {
					echo "<script>alert('Error en la carga del archivo'".basename($_FILES['userfile']['name'])."')</script>";
					$this->perfil_usuario();
				}

			}else{
				$data = $this->panel_Model->buscar_logs(SESSION("ci"));
				$vars["logs"] = $data;
				$this->title("Ver Contactos");
				$vars["view"] = $this->view("perfil",TRUE);
				$this->render("content",$vars);
				$logs = $this->panel_Model->logs("Ver Perfil");

			}
		}else{
			$this->login(); 
		}
	}
	public function horario_agregar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("registrar")){
				$data = $this->panel_Model->horario_agregar();
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->home();
			}else{
				$this->title("Registro Horario");
				$vars["carreras"]= $this->panel_Model->buscar_todos_carreras();
				$vars["aulas"]= $this->panel_Model->buscar_todos_aulas();
				$vars["materias"]= $this->panel_Model->buscar_todos_materias();
				$vars["secciones"]= $this->panel_Model->buscar_todos_secciones();
				$vars["usuarios"]= $this->panel_Model->buscar_todos_usuarios();
				$vars["periodos"]= $this->panel_Model->buscar_todos_periodos();
				$vars["view"] = $this->view("horarios_registrar",TRUE);
				$this->render("content",$vars);
			}
		}else{}
	}

	public function horarios_listas(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			$vars["horarios"] =$this->panel_Model->buscar_todos_horarios();
			$vars["view"] = $this->view("horarios_listas",TRUE);
			$this->render("content",$vars);
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}

	public function seccion_agregar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("registrar")){
				$data = $this->panel_Model->seccion_agregar();
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->home();
			}else{
				$this->title("Registro Seccion");
				$vars["carreras"]= $this->panel_Model->buscar_todos_carreras();
				$vars["view"] = $this->view("seccion_registrar",TRUE);
				$this->render("content",$vars);
			}
		}else{}
	}
	public function aula_agregar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("registrar")){
				$data = $this->panel_Model->aula_agregar();
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->home();
			}else{
				$this->title("Registro Aula");
				$vars["view"] = $this->view("aula_registrar",TRUE);
				$this->render("content",$vars);
			}
		}else{}
	}
	public function materia_agregar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("registrar")){
				$data = $this->panel_Model->materia_agregar();
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->home();
			}else{
				$this->title("Registro Aula");
				$vars["carreras"]= $this->panel_Model->buscar_todos_carreras();
				$vars["profesores"]= $this->panel_Model->buscar_todos_usuarios();
				$vars["view"] = $this->view("materia_registrar",TRUE);
				$this->render("content",$vars);
			}
		}else{}
	}
	public function materia_asignar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("registrar")){
				$data = $this->panel_Model->materia_asignar();
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->home();
			}else{
				$this->title("Registro Aula");
				$vars["materias"]= $this->panel_Model->buscar_todos_materias();
				$vars["periodo"]= $this->panel_Model->buscar_todos_periodos();
				$vars["profesores"]= $this->panel_Model->buscar_todos_usuarios();
				$vars["view"] = $this->view("materia_asignar",TRUE);
				$this->render("content",$vars);
			}
		}else{}
	}
	public function carrera_agregar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("registrar")){
				$data = $this->panel_Model->carrera_agregar();
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->home();
			}else{
				$this->title("Registro Carrera");
				$vars["profesores"]= $this->panel_Model->buscar_todos_usuarios();
				$vars["view"] = $this->view("carrera_registrar",TRUE);
				$this->render("content",$vars);
			}
		}else{}
	}
	public function periodo_agregar(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")){
			if(POST("registrar")){
				$data = $this->panel_Model->periodo_agregar();
				echo "<script>alert('Se ha guardado Correctamente');</script>";
				$this->home();
			}else{
				$this->title("Registro Periodo");
				$vars["view"] = $this->view("periodo_registrar",TRUE);
				$this->render("content",$vars);
			}
		}else{}
	}
	public function recuperar_clave(){
			if(POST("registro")){
				$data = $this->panel_Model->buscar_pregunta_usuario(POST("correo"));
				if($data[0]["respuesta"]==POST("respuesta")){
					$id=$data[0]["id"];
					$this->panel_Model->modificar_clave($id);
					echo "<script>alert('Se ha Modificado Correctamente');</script>";
					$this->login();
				}else{
					echo "<script>alert('La respuesta no es la misma');</script>";
				}
			}else{
				$this->title("Recuperar Clave");
				$vars["view"] = $this->view("recuperar",TRUE);
				$this->render("content",$vars);
			}	
	}
	public function listas_usuarios() {
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="users")||(SESSION("tipo_user")=="panelrio")){
			$data = $this->panel_Model->buscar_todos_usuarios();
			//____($data);
			if($data == false){
				echo "<script>alert('no hay usuarios en el sistema'); document.location.href='';</script>";
			}else{
				$this->title("Lista Usuarios");
				$vars["usuarios"] = $data;
				$vars["view"] = $this->view("listas_todos_usuarios",TRUE);
				$this->render("content",$vars);
				$logs = $this->panel_Model->logs("Ver Listas de Usuarios");
			}
		}else{
			$this->login(); 
		}
	}
	public function usuarios_options(){
		if( (SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="panelrio")){
			if(POST("borrar")){
				$data = $this->panel_Model->usuario_borrar(POST("libro_id"));
				echo "<script>alert('usuario Borrado con exito');</script>";
				$logs = $this->panel_Model->logs("Borrar Usuario");	
				$this->listas_usuarios();
			}elseif(POST("bloquear")){
				$data = $this->panel_Model->usuario_estado(POST("libro_id"),"bloqueado");
				$logs = $this->panel_Model->logs("Bloquear Usuario");
				$this->listas_usuarios();
			}elseif(POST("desbloquear")){
				$data = $this->panel_Model->usuario_estado(POST("libro_id"),"Activado");
				$logs = $this->panel_Model->logs("Desbloquear Usuario");
				$this->listas_usuarios();
			}
		}else{
			$this->login(); 
		}
	}
	public function usuario_borrar(){
		if(SESSION("tipo_user")=="admin"){
			if(POST("borrar")){
				$data = $this->panel_Model->usuario_borrar(POST("archivo_id"));
				//____($data);
				$logs = $this->panel_Model->logs("borrar contacto");
				echo "<script>alert('Se ha borrrado Correctamente');</script>";
				$this->listas_usuarios();
			}else{
				$this->title("Ver Contactos");
				$vars["view"] = $this->view("contactos/home",TRUE);
				$this->render("content",$vars);
			}
		}else{
			$this->login(); 
		}	
	}
	
	public function respaldar_bd(){
		if(SESSION("tipo_user")=="admin"){
			echo "<script> document.location.href='".get("webURL")."/www/applications/panel/controllers/backupdb.php'; alert('se ha guardado con exito')</script>";
			//$logs = $this->panel_Model->logs("Respaldar BD");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}
	public function reporte_aulas(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="panelrio")){	
			echo "<script> document.location.href='".get("webURL")."/www/classes/reporte_aulas.php';</script>";
			//$logs = $this->panel_Model->logs("Reporte aulas");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}	
	}
	public function reporte_carreras(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="panelrio")){	
			echo "<script> document.location.href='".get("webURL")."/www/classes/reporte_carreras.php';</script>";
			//$logs = $this->panel_Model->logs("Reporte Carreras");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}	
	}
	public function reporte_periodos(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="panelrio")){	
			echo "<script> document.location.href='".get("webURL")."/www/classes/reporte_periodos.php';</script>";
			//$logs = $this->panel_Model->logs("Reporte Periodos");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}	
	}
	public function reporte_materias(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="panelrio")){	
			echo "<script> document.location.href='".get("webURL")."/www/classes/reporte_materias.php';</script>";
			//$logs = $this->panel_Model->logs("Reporte Materias");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}	
	}
	public function reporte_usuarios(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="panelrio")){	
			echo "<script> document.location.href='".get("webURL")."/www/classes/reporte_usuarios.php';</script>";
			//$logs = $this->panel_Model->logs("Reporte Usuarios");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}	
	}
	public function reporte_logs(){
		if((SESSION("tipo_user")=="admin")||(SESSION("tipo_user")=="panelrio")){
			echo "<script> document.location.href='".get("webURL")."/www/classes/reporte_logs.php';</script>";
			//$logs = $this->panel_Model->logs("Reporte Logs");
			$this->home();
		}else{
			echo "<script>alert('Usted no posee los permisos necesarios')</script>";
			$this->home();
		}
	}

}
