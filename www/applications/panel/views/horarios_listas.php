<h2>Todas los horarios Cargados</h2>


<table class="table table-bordered datatable" id="table-1">
    <thead>
        <tr>
            <th>Hora</th>
            <th>Turno</th>
            <th>Periodo</th>
            <th>Modalidad</th>
            <th>Estado</th>
           <!--<th>Acciones</th>-->
        </tr>
    </thead>
    <tbody>
    <?php
        for($i=0; $i <= $horarios[$i]["id"]; $i++){
            
                    echo "<tr >";
                    echo "<td>".$horarios[$i]["hora"]."</td>";
                    echo "<td>".$horarios[$i]["turno"]."</td>";
                    echo "<td>".$horarios[$i]["periodo"]."</td>";
                    echo "<td>".$horarios[$i]["modalidad"]."</td>";
                    if(($horarios[$i]["status"]=="Activado")||($horarios[$i]["status"]=="activo")){
                    echo "<td> <a class='btn btn-lg btn-success '>".$horarios[$i]["status"]."</a></td>";
                    }else{
                    echo "<td> <a class='btn btn-lg btn-danger'>".$horarios[$i]["status"]."</a></td>";   
                    }
                    if(SESSION("tipo_user")=="admin"){
                    /*echo "<td>
                        <a data-id='".$horarios[$i]["id_empresa"]."' data-tipo='".$horarios[$i]["tipo"]."' data-descripcion='".$horarios[$i]["descripcion"]."' data-nombre='".$horarios[$i]["nombre"]."' href='#aprobar' class='open-aprobar btn btn-default btn-sm btn-icon icon-left'>
                            <i class='entypo-pencil'></i>
                            Aprobar
                        </a>
                        <a data-id='".$horarios[$i]["id_empresa"]."' href='#asignar' class='open-asignar btn btn-info btn-sm btn-icon icon-left'>
                            <i class='entypo-info'></i>
                            Asignar Palabra
                        </a>
                        <a data-id='".$horarios[$i]["id_empresa"]."' data-nombre='".$horarios[$i]["nombre_empresa"]."' href='#perfil' class='open-perfil btn btn-info btn-sm btn-icon icon-left'>
                            <i class='entypo-info'></i>
                            Ver Perfil Empresa
                        </a>
                        <a data-id='".$horarios[$i]["id_empresa"]."' href='#borrar' class='open-borrar btn btn-danger btn-sm btn-icon icon-left'>
                            <i class='entypo-cancel'></i>
                            Borrar
                        </a>
                         <a data-id='".$horarios[$i]["id_empresa"]."' href='#mapa' class='open-mapa btn btn-success btn-sm btn-icon icon-left'>
                            <i class='entypo-cancel'></i>
                            Agregar Mapa
                        </a>
                    </td>";*/
                    }else{
                        echo "<td>
                        <a data-id='".$horarios[$i]["id_empresa"]."' data-telefono='".$horarios[$i]["telefono"]."' data-servicio='".$horarios[$i]["servicio"]."' data-nombre='".$horarios[$i]["nombre"]."' href='#editar' class='open-edit btn btn-default btn-sm btn-icon icon-left'>
                            <i class='entypo-pencil'></i>
                            Editar
                        </a>
                        <a data-id='".$horarios[$i]["id_empresa"]."' data-nombre='".$horarios[$i]["nombre_empresa"]."' href='#perfil' class='open-perfil btn btn-info btn-sm btn-icon icon-left'>
                            <i class='entypo-info'></i>
                            Ver Perfil Empresa
                        </a>
                        <a data-id='".$horarios[$i]["id_empresa"]."' href='#borrar' class='open-borrar btn btn-danger btn-sm btn-icon icon-left'>
                            <i class='entypo-cancel'></i>
                            Borrar
                        </a>
                    </td>";
                    }
                    echo "</tr>";
                
            
        }
    ?>
    </tbody>
</table>
</div></div>
<div class="modal fade" id="aprobar">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Aprobar Empresa</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        
                        <div class="form-group">
                            <form action="<?php print path("panel/empresa_activar/") ?>" method="post">
                                <fieldset>
                                    <input type="text" style="visibility: hidden" name="empresa_id" id="id" size="10"value="" />
                                    
                                    <div class="form-group">
                                        <select class="form-control" name="status">
                                            <option>Activado</option>
                                            <option>Desactivado</option>
                                        </select>
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" name="activar_btn" type="submit" value="Modificar"/>                     
                                </fieldset>
                            </form>
                            
                        </div>  
                        
                    </div>
                </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

</div></div>


<div class="modal fade" id="editar">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Editar Empresa</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        
                        <div class="form-group">
                            <form action="<?php print path("panel/editar_horarios"); ?>" method="post" role="form" class="form-horizontal" >
                                <input type="text" style="visibility: hidden" name="libro_id" id="bookId" size="10"value="" />     
                                </br>
                                <input class="form-control" name="nombres" type="text" id="bookname"  placeholder="Razon Social" required="required" autofocus>
                                </br>
                                <input class="form-control" name="rif"  type="text" placeholder="RIF" required="required">
                                </br>
                                <select class="form-control" name="estado">
                                  <option>Elige un Estado</option>
                                  <option>Amazonas</option>
                                  <option>Anzoategui</option>
                                  <option>Apure</option>
                                  <option>Aragua</option>
                                  <option>Barinas</option>
                                  <option>Bolivar</option>
                                  <option>Carabobo</option>
                                  <option>Cojedes</option>
                                  <option>Delta Amacuro</option>
                                  <option>Distrito Capital</option>
                                  <option>Falcon</option>
                                  <option>Guarico </option>
                                  <option>Lara</option>
                                  <option>Merida</option>
                                  <option>Monagas</option>
                                  <option>Nueva Esparta</option>
                                  <option>Portuguesa</option>
                                  <option>Sucre</option>
                                  <option>Tachira</option>
                                  <option>Trujillo</option>
                                  <option>Vargas</option>
                                  <option>Yaracuy</option>
                                  <option>Zulia</option>
                                </select>           
                                </br>
                                <input class="form-control" name="direccion" type="text" placeholder="Direccion" required="required">
                                </br>
                                <select class="form-control" name="categoria">
                                  <option>Seleccione una Categoria</option>
                                    <?php 
                                        for($i=0; $i <= $categorias[$i]["id"]; $i++){
                                            echo "<option>".$categorias[$i]["nombre"]."<option>";
                                        }
                                    ?>
                                </select>           
                                </br>
                                <input class="form-control" name="telefono" type="phone" placeholder="Número Telefónico" value="<script>myBookId</script>"required="required">           
                                </br>
                                <input class="form-control" name="email" type="mail" placeholder="Correo ejem: correo@example.com" required="required">         
                                </br>
                                <input class="form-control" name="facebook" type="url" placeholder="Facebook ejm: https://facebook.com/usuario" required="required">            
                                </br>
                                <input class="form-control" name="instagram" type="url" placeholder="instagram ejm: https://instagram.com/usuario" required="required">
                                </br>
                                <input class="form-control" name="pagina" type="url" placeholder="website ejm: http://dominio.com" required="required">         
                                </br>
                                <input class="form-control" name="twitter" type="url" placeholder="Twitter ejm: https://twitter.com/usuario#" required="required">          
                                </br>
                                <input class="form-control" name="horarios" type="text" placeholder="Horarios" required="required">         
                                </br>
                                <select class="form-control" name="tipo_pago">
                                  <option>Elige un Tipo de Pago</option>
                                  <option>Efectivo</option>
                                  <option>Cheque</option>
                                  <option>Electronico</option>
                                  <option>Transferencias</option>
                                  <option>Tarjetas de Credito</option>
                                  <option>Tarjetas de Debito</option>
                                  <option>Todas</option>
                                  <option>Todas menos Cheque</option>
                                  <option>Debito, Credito, Efectivo</option>
                                  <option>Efectivo, Debito</option>
                                  <option>Debito, Credito</option>
                                </select>
                                <br />
                                <textarea class="form-control" name="servicio" type="text" placeholder="Servicios" required="required">Describe Todos tus Servicios</textarea>      
                                </br>
                                <input class="btn btn-lg btn-success btn-block" name="modificar" type="submit" value="Modificar"/>               
                            </form>
                        </div>  
                        
                    </div>
                </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
</div></div>

<div class="modal fade" id="asignar">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Asignar Palabra a Empresa</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        
                        <div class="form-group">
                            <form action="<?php print path("panel/agregar_palabra_empresa/") ?>" method="post">
                                <fieldset>
                                    <input type="text" style="visibility: hidden" name="empresa_id" id="Id" size="10"value="" />
                                    
                                    <div class="form-group">
                                        <select class="form-control" name="palabra">
                                           <option>Seleccione una Palabra</option>
                                           <?php 
                                                for($i=0; $i <= $palabras[$i]["id"]; $i++){
                                                    echo "<option>".$palabras[$i]["palabra"]."<option>";
                                                }

                                           ?>
                                        </select>
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" name="agregar_palabra_btn" type="submit" value="Asignar Palabra"/>                     
                                </fieldset>
                            </form>
                            
                        </div>  
                        
                    </div>
                </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
</div></div>

<div class="modal fade" id="perfil">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Ver Perfil Empresa</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        
                        <div class="form-group">
                            <form action="<?php print path("panel/ver_perfil_empresa/") ?>" method="post">
                                <fieldset>
                                    <input type="text" style="visibility: hidden" name="empresa_id" id="Idempre" size="10"value="" />
                                    <div class="form-group">
                                        <p> Desea ver el perfil de la empresa <strong><a id="nombre_empresa"></a></strong>, en ella esta la informacion detallada.</p>
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" name="ver_perfil_btn" type="submit" value="ir a Perfil"/>                     
                                </fieldset>
                            </form>
                            
                        </div>  
                        
                    </div>
                </div>
        </div>
    </div>
</div>
</div></div>

<div class="modal fade" id="borrar">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Borrar Empresa</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        
                        <div class="form-group">
                            <p align="center">¿Quieres Borrar la Empresa? una vez Borrado no aparecera mas en tu panel</p>
                            
                        </div>  
            
                    </div>
                </div>
    
            <div class="modal-footer">
                <form action="<?php print path("panel/borrar_empresa/") ?>" method="post">
                    <input type="text" style="visibility: hidden" name="empresa_id" id="Idempresa" size="10"value="" />
                    <input type="submit" class="btn btn-default" name="borrar_btn" value="Aceptar"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </form>
            </div>
        </div>
    </div>
</div>

</div></div>

<div class="modal fade" id="mapa">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Agregar Mapa a Empresa</h4>
            </div>
            
            <div class="modal-body">
            
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                      <form action="<?php print path("panel/mapa_empresa_agregar/") ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                          <input class="form-control" name="bookfile" type="file" required >Mapa de la empresa</input>
                            
                        </div>  
            
                    </div>
                </div>
    
            <div class="modal-footer">
                    <input type="text" style="visibility: hidden" name="empresa_id" id="Idempresamapa" size="10"value="" />
                    <input type="submit" class="btn btn-default" name="agregar_btn" value="Aceptar"/>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($)
    {
        $("#table-1").dataTable({
            "sPaginationType": "bootstrap",
            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "bStateSave": true
        });
        
        
    });

    jQuery(document).on("click", ".open-aprobar", function (e) {
        e.preventDefault();
        var _self = $(this);
        var myBookId = _self.data('id');
        var myBookname = _self.data('nombre');
        var myBookdescription = _self.data('servicio');
        var myBooktags = _self.data('telefono');
        
        $("#id").val(myBookId);
        $("#nombre").val(myBookname);
        $("#servicio").val(myBookdescription);
        $("#telefono").val(myBooktags);

        $(_self.attr('href')).modal('show',{backdrop: 'static'});
        //alert(myBookId1);
    });

    jQuery(document).on("click", ".open-edit", function (e) {
        e.preventDefault();
        var _self = $(this);
        var myBookId = _self.data('id');
        var myBookname = _self.data('nombre');
        var myBookdescription = _self.data('servicio');
        var myBooktags = _self.data('telefono');
        
        $("#id").val(myBookId);
        $("#nombre").val(myBookname);
        $("#servicio").val(myBookdescription);
        $("#telefono").val(myBooktags);

        $(_self.attr('href')).modal('show',{backdrop: 'static'});
        //alert(myBookId1);
    });


    jQuery(document).on("click", ".open-asignar", function (e) {
        e.preventDefault();
        var _self = $(this);
        var myBookId = _self.data('id');
        $("#Id").val(myBookId);
        $(_self.attr('href')).modal('show',{backdrop: 'static'});
        //alert(myBookId);
    });

    jQuery(document).on("click", ".open-perfil", function (e) {
        e.preventDefault();
        var _self = $(this);
        var myBookId = _self.data('id');
        var myBookname = _self.data('nombre');

        $("#Idempre").val(myBookId);
        $("#nombre_empresa").html(myBookname);
        $(_self.attr('href')).modal('show',{backdrop: 'static'});
    });

    jQuery(document).on("click", ".open-borrar", function (e) {
        e.preventDefault();
        var _self = $(this);
        var myBookId = _self.data('id');
        
        $("#Idempresa").val(myBookId);
        $(_self.attr('href')).modal('show',{backdrop: 'static'});
        //alert(myBookId);
    });
     jQuery(document).on("click", ".open-mapa", function (e) {
        e.preventDefault();
        var _self = $(this);
        var myBookId = _self.data('id');
        
        $("#Idempresamapa").val(myBookId);
        $(_self.attr('href')).modal('show',{backdrop: 'static'});
        //alert(myBookId);
    });
</script>