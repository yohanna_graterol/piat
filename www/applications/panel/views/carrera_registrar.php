<section id="content" class="m-t-lg wrapper-md animated fadeInDown"> 
    <div class="container aside-xl"> 
      <section class="m-b-lg"> 
        <h3 align="center">Registro de Carrera</h3>
        <form action="<?php print path("panel/carrera_agregar"); ?>" method="post"> 
          <div class="list-group">
            <div class="list-group-item"> 
              <select class="form-control no-border" name="id_jefe_proyecto">
                <option>Elije Jefe de Carrera</option>
                <?php
                    for($i=0; $i <= $profesores[$i]["id"]; $i++){
                      echo "<option value=".$profesores[$i]["id"].">CI:".$profesores[$i]["cedula"].": ".$profesores[$i]["nombres"].", ".$profesores[$i]["apellidos"]."</option>";
                    }
                ?>
              </select>
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Nombre" class="form-control no-border" name="nombre"> 
            </div>
            <div class="list-group-item"> 
              <input type="text" placeholder="Descripcion" class="form-control no-border" name="descripcion"> 
            </div>
            <div class="list-group-item"> 
              <input type="text" placeholder="Total de Materias" class="form-control no-border" name="num_materias"> 
            </div>
            <div class="list-group-item"> 
              <input type="text" placeholder="Total de horas" class="form-control no-border" name="total_horas"> 
            </div>
            <div class="list-group-item"> 
              <input type="text" placeholder="Total de Unidades de Credito" class="form-control no-border" name="uc"> 
            </div>
            <input type="submit" class="btn btn-lg btn-primary btn-block"value="Registar" name="registrar"> 
          <div class="line line-dashed"></div>  
        </form> 
      </section> 
    </div> 
  </section> 
