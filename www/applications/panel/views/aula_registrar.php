  <section id="content" class="m-t-lg wrapper-md animated fadeInDown"> 
    <div class="container aside-xl"> 
      <section class="m-b-lg"> 
        <h3 align="center">Registro de Aula</h3>
        <form action="<?php print path("panel/aula_agregar"); ?>" method="post"> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Numero de Pabellon" class="form-control no-border" name="numero_pabellon"> 
            </div>
            <div class="list-group-item"> 
              <input type="text" placeholder="Nombre de Salon" class="form-control no-border" name="numero_salon"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Descripcion" class="form-control no-border" name="descripcion"> 
            </div>
            <input type="submit" class="btn btn-lg btn-primary btn-block"value="Registar" name="registrar"> 
          <div class="line line-dashed"></div>  
        </form> 
      </section> 
    </div> 
  </section> 