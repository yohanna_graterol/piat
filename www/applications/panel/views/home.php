<?php if(){}?>
<h2>Todos los Profesores Registrados por <?php print SESSION("nombre")?>, <?php print SESSION("apellido")?></h2>

<br />

<table class="table table-bordered datatable" id="table-1">
    <thead>
        <tr>
            <th>Numero</th>
            <th>CI</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Telefono</th>
            <th>Correo</th>
            <th>estado</th>
            <th>Tipo de Contrato</th>
            <th>Turno</th>
            <th>Modalidad</th>
        </tr>
    </thead>
    <tbody>
    <?php
        for($i=0; $i <= $profesores[$i]["id"]; $i++){
           
            echo "<tr >";
            echo "<td>".$i."</td>";
            echo "<td>".$profesores[$i]["cedula"]."</td>";
            echo "<td>".$profesores[$i]["nombres"]."</td>";
            echo "<td>".$profesores[$i]["apellidos"]."</td>";
            echo "<td>".$profesores[$i]["telefono"]."</td>";
            echo "<td>".$profesores[$i]["correo"]."</td>";
            if(($profesores[$i]["estatus"]=="Activado")||($profesores[$i]["estatus"]=="activo")){
                echo "<td> <a class='btn btn-lg btn-success'>".$profesores[$i]["estatus"]."</a></td>";
            }else{
                echo "<td> <a class='btn btn-lg btn-danger'>".$profesores[$i]["estatus"]."</a></td>";   
            }
            echo "<td>".$profesores[$i]["tipo_contrato"]."</td>";
            echo "<td>".$profesores[$i]["turno"]."</td>";
            echo "<td>".$profesores[$i]["modalidad"]."</td>";
        }
    ?>
    </tbody>
</table>
</div></div>