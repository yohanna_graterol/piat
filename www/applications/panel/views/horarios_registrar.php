<section id="content" class="m-t-lg wrapper-md animated fadeInDown"> 
    <div class="container aside-xl"> 
      <section class="m-b-lg"> 
        <h3 align="center">Registro de Horario</h3>
        <form action="<?php print path("panel/horario_agregar"); ?>" method="post"> 
          <div class="list-group"> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Hora" class="form-control no-border" name="hora"> 
            </div> 
             <div class="list-group-item"> 
              <select class="form-control no-border" name="periodo">
                <option>Elije la Periodo</option>
                <?php
                    for($i=0; $i <= $periodos[$i]["id"]; $i++){
                      echo "<option value=".$periodos[$i]["id"].">".$periodos[$i]["nombre"]."</option>";
                    }
                ?>
              </select>
            </div>
            <div class="list-group-item"> 
              <select class="form-control no-border" name="turno">
                <option>Elije el tipo de Turno</option>
                <option value="D">Diurno</option>
                <option value="N">Nocturno</option>
                <option value="FS">Fines de semana</option>

              </select>
            </div> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="modalidad">
                <option>Elije el tipo de Modalidad</option>
                <option value="R">Regular</option>
                <option value="TUT">Tutorias</option>
                <option value="PG">Prueba global</option>
                <option value="">SI</option>
                <option value="">CP</option>
              </select> 
            </div> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="id_carrera" >
                <option>Elije la Carrera</option>
                <?php
                    for($i=0; $i <= $carreras[$i]["id"]; $i++){
                      echo "<option value=".$carreras[$i]["id"].">".$carreras[$i]["nombre"]."</option>";
                    }
                ?>
              </select> 
            </div> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="id_materia">
                <option>Elije la Materia</option>
                <?php
                    for($i=0; $i <= $materias[$i]["id"]; $i++){
                      echo "<option value=".$materias[$i]["id"].">".$materias[$i]["nombre"]."</option>";
                    }
                ?>
              </select>
            </div> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="id_profesor">
                <option>Elije el Profesor</option>
               <?php
                    for($i=0; $i <= $usuarios[$i]["id"]; $i++){
                       echo "<option value=".$usuarios[$i]["id"].">C.I:".$usuarios[$i]["cedula"]." - ".$usuarios[$i]["nombres"].", ".$usuarios[$i]["apellidos"]."</option>";
                    }
                ?>
              </select> 
            </div>
            <div class="list-group-item"> 
              <select class="form-control no-border" name="id_seccion">
                <option>Elije la Seccion</option>
                <?php
                    for($i=0; $i <= $secciones[$i]["id"]; $i++){
                      echo "<option value=".$secciones[$i]["id"].">".$secciones[$i]["nombre"]."</option>";
                    }
                ?>
              </select> 
            </div>
            <div class="list-group-item"> 
              <select class="form-control no-border" name="id_aula">
                <option>Elije el aula</option>
                <?php
                    for($i=0; $i <= $aulas[$i]["id"]; $i++){
                      echo "<option value=".$aulas[$i]["id"]."> Aula:".$aulas[$i]["numero_salon"]." - Pabellon:".$aulas[$i]["numero_pabellon"]."</option>";
                    }
                ?>
              </select> 
            </div>
            <input type="submit" class="btn btn-lg btn-primary btn-block"value="Registar" name="registrar"> 
          <div class="line line-dashed"></div>  
        </form> 
      </section> 
    </div> 
  </section> 
