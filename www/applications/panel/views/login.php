<body class="page-body ">
<div class="login-container">
  <div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <div class="login-content" align="center">
      <a href="<?php print path("login/index/"); ?>" class="logo">
        <img src="<?php print $this->themePath; ?>/images/logo.png" height="90" width="110" alt="logo institucion" />
      </a>
      <p class="description">Acceso para Usuarios Registrados</p> 
    </div>
    
  </div>
  
  <section id="content" class="m-t-lg wrapper-md animated fadeInDown"> 
    <div class="container aside-xl"> 
      <section class="m-b-lg"> 
        <h3 align="center">Entrar al Sistema</h3>

      <form method="post" action="<?php print path("panel/login/"); ?>" >
        <div class="form-group panel">    
          <div class="input-group">
            <div class="input-group-addon">
              <i class="entypo-user"></i>
            </div>
            <input type="text" class="form-control" name="usuario"  placeholder="Usuario" autocomplete="off" required autofous/>
          </div>
        </div>
        
        <div class="form-group panel">
          <div class="input-group">
            <div class="input-group-addon">
              <i class="entypo-key"></i>
            </div>
            <input type="password" class="form-control" name="clave" placeholder="Contraseña" autocomplete="off" required/>
          </div>
        </div>
        
        <div class="form-group">
          <input type="submit" class="btn btn-lg btn-success btn-block" name="entrar" value="Entrar">
        </div>
      </form>
      </section> 
    </div> 
  </section> 
</div>