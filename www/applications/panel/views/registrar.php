<?php if($tipo=="profesor"){ ?>
<section id="content" class="m-t-lg wrapper-md animated fadeInDown"> 
    <div class="container aside-xl"> 
      <section class="m-b-lg"> 
        <h3 align="center">Registro Profesor</h3>

<form id="rootwizard-2" method="post" action="<?php print path("panel/registrar"); ?>" class="form-wizard validate" novalidate="novalidate" enctype="multipart/form-data">
  
  <div class="steps-progress" style="margin-left: 101.5px; margin-right: 101.5px;">
    <div class="progress-indicator" style="width: 0px;"></div>
  </div>
  
  <ul>
    <li class="active">
      <a href="#tab2-1" data-toggle="tab"><span>1</span>Datos Personales</a>
    </li>
    <li>
      <a href="#tab2-2" data-toggle="tab"><span>2</span>Contrato</a>
    </li>
    <li>
      <a href="#tab2-3" data-toggle="tab"><span>3</span>Pre Grado</a>
    </li>
    <li>
      <a href="#tab2-4" data-toggle="tab"><span>4</span>Especialización</a>
    </li>
    <li>
      <a href="#tab2-5" data-toggle="tab"><span>5</span>Maestria</a>
    </li>
    <li>
      <a href="#tab2-6" data-toggle="tab"><span>6</span>Doctorado</a>
    </li>
  </ul>
  
  <div class="tab-content" style="margin-left: 84px; margin-right: 84px;">
    <div class="tab-pane active" id="tab2-1">
      
         <div class="list-group"> 

            <div class="list-group-item"> 
              <select class="form-control no-border" name="nacionalidad" >
                <option>Nacionalidad</option>
                <option value="V">Venezonalo</option>
                <option value="P">Extranjero</option>
              </select> 
            </div> 
            
            <div class="list-group-item"> 
              <input type="text" placeholder="Cedula" class="form-control no-border" name="cedula" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Fecha de Nacimiento" class="form-control no-border" name="fecha_nac" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Nombre" class="form-control no-border" name="nombres" required>  
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Apellidos" class="form-control no-border" name="apellidos" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Telefono" class="form-control no-border" name="telefono" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="email" placeholder="Correo Electronico" class="form-control no-border" name="correo" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Direccion" class="form-control no-border" name="direccion" required> 
            </div> 
          </div>
    </div>
    
    <div class="tab-pane" id="tab2-2">
          <div class="list-group"> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="tipo_contrato" >
                <option>Elije el tipo de contrato</option>
                <option>Contratado</option>
                <option>Fijo</option>
              </select> 
            </div> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="turno">
                <option>Elije el tipo de Turno</option>
                <option value="D">Diurno</option>
                <option value="N">Nocturno</option>
                <option value="FS">Fines de semana</option>

              </select>
            </div> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="modalidad">
                <option>Elije el tipo de Modalidad</option>
                <option value="R">Regular</option>
                <option value="TUT">Tutorias</option>
                <option value="PG">Prueba global</option>
                <option value="">SI</option>
                <option value="">CP</option>
              </select> 
            </div>
          </div>
        
           <div class="list-group-item"> 
              <input type="text" placeholder="Categoria Inicial" class="form-control no-border" name="categoria_inicial"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Categoria Actual" class="form-control no-border" name="categoria_actual"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Dedicacion Inicial" class="form-control no-border" name="dedicacion_inicial"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Fecha de Ingreso" class="form-control no-border" name="fecha_ingreso" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Fecha del Ultimo Ascenso" class="form-control no-border" name="fecha_ultimo_ascenso"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Fecha Proximo Ascenso" class="form-control no-border" name="fecha_proximo_ascenso"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Fecha de Posible Jubilacion" class="form-control no-border" name="fecha_probable_jubilacion"> 
            </div> 

    </div>

    <div class="tab-pane" id="tab2-3">
      
        <div class="list-group-item"> 
              <input type="text" placeholder="Titulo Pregrado" class="form-control no-border" name="titulo_pregado" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Universidad Pregrado" class="form-control no-border" name="universidad_pregrado" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Año del grado" class="form-control no-border" name="ano_grado" required> 
            </div> 
      
    </div>
    <div class="tab-pane" id="tab2-4">
        
            <div class="list-group-item"> 
              <input type="text" placeholder="Titulo especializacion" class="form-control no-border" name="titulo_especializacion"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Universidad especializacion" class="form-control no-border" name="universidad_especializacion"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Año del Grado" class="form-control no-border" name="ano_especializacion"> 
            </div> 
      
    </div>
    <div class="tab-pane" id="tab2-5">
      
       <div class="list-group-item"> 
              <input type="text" placeholder="Titulo Maestria" class="form-control no-border" name="titulo_maestria"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Universidad Maestria" class="form-control no-border" name="universidad_maestria"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Año del Grado" class="form-control no-border" name="ano_maestria"> 
            </div> 
    </div>
    <div class="tab-pane" id="tab2-6">

        <div class="list-group-item"> 
              <input type="text" placeholder="Titulo Doctorado" class="form-control no-border" name="titulo_doctorado"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Universidad Doctorado" class="form-control no-border" name="universidad_doctorado"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Año del Grado" class="form-control no-border" name="ano_doctorado"> 
            </div> 
      
            <input type="submit" class="btn btn-lg btn-primary btn-block large" name="registrar" value="Registrar" />
            
    </div>
          
          <ul class="pager wizard">
            <li class="previous disabled">
              <a href="#"><i class="entypo-left-open"></i> Anterior</a>
            </li>
            
            <li class="next">
              <a href="#">Siguiente <i class="entypo-right-open"></i></a>
            </li>
          </ul>
        

        </div>
      </form>
      </section> 
    </div> 
  </section> 
<?php }elseif($tipo=="usuario"){ ?>
<section id="content" class="m-t-lg wrapper-md animated fadeInDown"> 
    <div class="container aside-xl"> 
      <section class="m-b-lg"> 
        <h3 align="center">Registro Profesor Coordinador</h3>


<form id="rootwizard-2" method="post" action="<?php print path("panel/registrar"); ?>" class="form-wizard validate" novalidate="novalidate" enctype="multipart/form-data">
  
  <div class="steps-progress" style="margin-left: 101.5px; margin-right: 101.5px;">
    <div class="progress-indicator" style="width: 0px;"></div>
  </div>
  
  <ul>
    <li class="active">
      <a href="#tab2-1" data-toggle="tab"><span>1</span>Datos Personales</a>
    </li>
    <li>
      <a href="#tab2-2" data-toggle="tab"><span>2</span>Contrato</a>
    </li>
    <li>
      <a href="#tab2-3" data-toggle="tab"><span>3</span>Pre Grado</a>
    </li>
    <li>
      <a href="#tab2-4" data-toggle="tab"><span>4</span>Especialización</a>
    </li>
    <li>
      <a href="#tab2-5" data-toggle="tab"><span>5</span>Maestria</a>
    </li>
    <li>
      <a href="#tab2-6" data-toggle="tab"><span>6</span>Doctorado</a>
    </li>
      <li>
      <a href="#tab2-7" data-toggle="tab"><span>7</span>Usuario</a>
    </li>
  </ul>
  
  <div class="tab-content" style="margin-left: 84px; margin-right: 84px;">
    <div class="tab-pane active" id="tab2-1">
      
         <div class="list-group"> 

            <div class="list-group-item"> 
              <select class="form-control no-border" name="nacionalidad" >
                <option>Nacionalidad</option>
                <option value="V">Venezonalo</option>
                <option value="P">Extranjero</option>
              </select> 
            </div> 
            
            <div class="list-group-item"> 
              <input type="text" placeholder="Cedula" class="form-control no-border" name="cedula" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Fecha de Nacimiento" class="form-control no-border" name="fecha_nac" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Nombre" class="form-control no-border" name="nombres" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Apellidos" class="form-control no-border" name="apellidos" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Telefono" class="form-control no-border" name="telefono" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="email" placeholder="Correo Electronico" class="form-control no-border" name="correo" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Direccion" class="form-control no-border" name="direccion" required> 
            </div> 
          </div>
    </div>
    
    <div class="tab-pane" id="tab2-2">
          <div class="list-group"> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="tipo_contrato" >
                <option>Elije el tipo de contrato</option>
                <option>Contratado</option>
                <option>Fijo</option>
              </select> 
            </div> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="turno">
                <option>Elije el tipo de Turno</option>
                <option value="D">Diurno</option>
                <option value="N">Nocturno</option>
                <option value="FS">Fines de semana</option>

              </select>
            </div> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="modalidad">
                <option>Elije el tipo de Modalidad</option>
                <option value="R">Regular</option>
                <option value="TUT">Tutorias</option>
                <option value="PG">Prueba global</option>
                <option value="">SI</option>
                <option value="">CP</option>
              </select> 
            </div>
          </div>
        
           <div class="list-group-item"> 
              <input type="text" placeholder="Categoria Inicial" class="form-control no-border" name="categoria_inicial"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Categoria Actual" class="form-control no-border" name="categoria_actual"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Dedicacion Inicial" class="form-control no-border" name="dedicacion_inicial"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Fecha de Ingreso" class="form-control no-border" name="fecha_ingreso" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Fecha del Ultimo Ascenso" class="form-control no-border" name="fecha_ultimo_ascenso"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Fecha Proximo Ascenso" class="form-control no-border" name="fecha_proximo_ascenso"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Fecha de Posible Jubilacion" class="form-control no-border" name="fecha_probable_jubilacion"> 
            </div> 

    </div>

    <div class="tab-pane" id="tab2-3">
      
        <div class="list-group-item"> 
              <input type="text" placeholder="Titulo Pregrado" class="form-control no-border" name="titulo_pregado" required>  
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Universidad Pregrado" class="form-control no-border" name="universidad_pregrado" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Año del grado" class="form-control no-border" name="ano_grado" required> 
            </div> 
      
    </div>
    <div class="tab-pane" id="tab2-4">
        
            <div class="list-group-item"> 
              <input type="text" placeholder="Titulo especializacion" class="form-control no-border" name="titulo_especializacion"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Universidad especializacion" class="form-control no-border" name="universidad_especializacion"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Año del Grado" class="form-control no-border" name="ano_especializacion"> 
            </div> 
      
    </div>
    <div class="tab-pane" id="tab2-5">
      
       <div class="list-group-item"> 
              <input type="text" placeholder="Titulo Maestria" class="form-control no-border" name="titulo_maestria"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Universidad Maestria" class="form-control no-border" name="universidad_maestria"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Año del Grado" class="form-control no-border" name="ano_maestria"> 
            </div> 
    </div>
    <div class="tab-pane" id="tab2-6">

        <div class="list-group-item"> 
              <input type="text" placeholder="Titulo Doctorado" class="form-control no-border" name="titulo_doctorado"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Universidad Doctorado" class="form-control no-border" name="universidad_doctorado"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Año del Grado" class="form-control no-border" name="ano_doctorado"> 
            </div> 
            
    </div>
    <div class="tab-pane" id="tab2-7">
       <div class="list-group-item"> 
              <input type="" placeholder="Usuario" class="form-control no-border" name="usuario" required> 
            </div> 
            <div class="list-group-item"> 
              <input type="password" placeholder="Clave" class="form-control no-border" name="clave" required> 
            </div> 
            <input type="submit" class="btn btn-lg btn-primary btn-block large" name="registrar" value="Registrar" />
    </div>  
          <ul class="pager wizard">
            <li class="previous disabled">
              <a href="#"><i class="entypo-left-open"></i> Anterior</a>
            </li>
            
            <li class="next">
              <a href="#">Siguiente <i class="entypo-right-open"></i></a>
            </li>
          </ul>
        

        </div>
      </form>




      </section> 
    </div> 
  </section> 
<?php } ?>

