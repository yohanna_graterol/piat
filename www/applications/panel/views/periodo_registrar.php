<section id="content" class="m-t-lg wrapper-md animated fadeInDown"> 
    <div class="container aside-xl"> 
      <section class="m-b-lg"> 
        <h3 align="center">Registro de Periodo</h3>
        <form action="<?php print path("panel/periodo_agregar"); ?>" method="post"> 
          <div class="list-group"> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Nombre Periodo" class="form-control no-border" name="nombre"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Descripcion" class="form-control no-border" name="descripcion"> 
            </div>
            <input type="submit" class="btn btn-lg btn-primary btn-block"value="Registar" name="registrar"> 
          <div class="line line-dashed"></div>  
        </form> 
      </section> 
    </div> 
  </section> 
