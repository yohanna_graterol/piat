<section id="content" class="m-t-lg wrapper-md animated fadeInDown"> 
    <div class="container aside-xl"> 
      <section class="m-b-lg"> 
        <h3 align="center">Registro de seccion</h3>
        <form action="<?php print path("panel/seccion_agregar"); ?>" method="post"> 
          <div class="list-group"> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="id_carrera">
                <option>Elije la Carrera</option>
                <?php
                    for($i=0; $i <= $carreras[$i]["id"]; $i++){
                      echo "<option value=".$carreras[$i]["id"].">".$carreras[$i]["nombre"]."</option>";
                    }
                ?>
              </select>
            </div> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="turno">
                <option>Elije el Turno</option>
                <option value="D">Diurno</option>
                <option value="N">Nocturno</option>
                <option value="FS">Fines de Semana</option>
              </select>
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Nombre Seccion" class="form-control no-border" name="nombre"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Descripcion" class="form-control no-border" name="descripcion"> 
            </div>
            <input type="submit" class="btn btn-lg btn-primary btn-block"value="Registar" name="registrar"> 
          <div class="line line-dashed"></div>  
        </form> 
      </section> 
    </div> 
  </section> 
