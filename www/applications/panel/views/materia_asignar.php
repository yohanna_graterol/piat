<section id="content" class="m-t-lg wrapper-md animated fadeInDown"> 
    <div class="container aside-xl"> 
      <section class="m-b-lg"> 
        <h3 align="center">Asignar Materia a Profesor</h3>
        <form action="<?php print path("panel/materia_asignar"); ?>" method="post"> 
          <div class="list-group"> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="id_profesor">
                <option>Elije Profesor</option>
                <?php
                    for($i=0; $i <= $profesores[$i]["id"]; $i++){
                      echo "<option value=".$profesores[$i]["id"].">".$profesores[$i]["nombres"].", ".$profesores[$i]["apellidos"]."</option>";
                    }
                ?>
              </select>
            </div>
            <div class="list-group-item"> 
              <select class="form-control no-border" name="id_carrera">
                <option>Elije la Materia</option>
                <?php
                    for($i=0; $i <= $materias[$i]["id"]; $i++){
                      echo "<option value=".$materias[$i]["id"].">".$materias[$i]["nombre"]."</option>";
                    }
                ?>
              </select>
            </div> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="id_periodo">
                <option>Elije Periodo</option>
                <?php
                    for($i=0; $i <= $periodo[$i]["id"]; $i++){
                      echo "<option value=".$periodo[$i]["id"].">".$periodo[$i]["nombre"]."</option>";
                    }
                ?>
              </select>
            </div>  
            <div class="list-group-item"> 
              <input type="text" placeholder="Descripcion" class="form-control no-border" name="descripcion"> 
            </div>
            <input type="submit" class="btn btn-lg btn-primary btn-block"value="Registar" name="registrar"> 
          <div class="line line-dashed"></div>  
        </form> 
      </section> 
    </div> 
  </section> 


