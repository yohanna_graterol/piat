<section id="content" class="m-t-lg wrapper-md animated fadeInDown"> 
    <div class="container aside-xl"> 
      <section class="m-b-lg"> 
        <h3 align="center">Registro de Materia</h3>
        <form action="<?php print path("panel/materia_agregar"); ?>" method="post"> 
          <div class="list-group"> 
            <div class="list-group-item"> 
              <select class="form-control no-border" name="id_carrera">
                <option>Elije la Carrera</option>
                <?php
                    for($i=0; $i <= $carreras[$i]["id"]; $i++){
                      echo "<option value=".$carreras[$i]["id"].">".$carreras[$i]["nombre"]."</option>";
                    }
                ?>
              </select>
            </div>
            <div class="list-group-item"> 
              <select class="form-control no-border" name="id_jefe_proyecto">
                <option>Elije Jefe de Materia</option>
                <?php
                    for($i=0; $i <= $profesores[$i]["id"]; $i++){
                      echo "<option value=".$profesores[$i]["id"].">".$profesores[$i]["nombres"].", ".$profesores[$i]["apellidos"]."</option>";
                    }
                ?>
              </select>
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Codigo Materia" class="form-control no-border" name="codigo"> 
            </div> 
            <div class="list-group-item"> 
              <input type="text" placeholder="Nombre" class="form-control no-border" name="nombre"> 
            </div>
            <div class="list-group-item"> 
              <input type="text" placeholder="Descripcion" class="form-control no-border" name="descripcion"> 
            </div>
            <div class="list-group-item"> 
              <select class="form-control no-border" id="ht"name="ht">
                <option>Elije Horas Teoricas</option>
                <option>0</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
                <option>11</option>
                <option>12</option>
              </select>
            </div>
            <div class="list-group-item"> 
              <select class="form-control no-border" id="htp" name="htp">
                <option>Elije Horas Teorico Practico</option>
                <option>0</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
                <option>11</option>
                <option>12</option>
              </select>
            </div>
            <div class="list-group-item"> 
              <select class="form-control no-border" id="hp" name="hp">
                <option>Elije Horas Practicas</option>
                <option>0</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
                <option>11</option>
                <option>12</option>
              </select>
            </div>
            <div class="list-group-item"> 
              <input type="text" placeholder="total de horas"id="Importe" class="form-control no-border" name="total_horas"> 
            </div>
            <div class="list-group-item"> 
              <select class="form-control no-border" name="uc">
                <option>Elije Cantidad de Unidades de Credito</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
              </select> 
            </div>
            <input type="submit" class="btn btn-lg btn-primary btn-block"value="Registar" name="registrar"> 
          <div class="line line-dashed"></div>  
        </form> 
      </section> 
    </div> 
  </section> 
<script type="text/javascript">
  $(document).ready(function(){
    var ht = 0;
    var htp = 0;
    var hp = 0;
    $("#hp").change(function(){
      ht = parseInt($("#ht").val())
      htp = parseInt($("#htp").val())
      hp=parseInt($("#hp").val())
      var th = (ht + htp ) + hp
      $("#Importe").val(th); 
    });                
  });
</script>