CREATE DATABASE IF NOT EXISTS piat;

USE piat;

DROP TABLE IF EXISTS aulas;

CREATE TABLE `aulas` (
  `id` int(255) NOT NULL auto_increment,
  `numero_pabellon` varchar(2) collate utf8_spanish2_ci NOT NULL,
  `numero_salon` varchar(2) collate utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `fecha_registro` varchar(12) collate utf8_spanish2_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO aulas VALUES("1","1","2","En perfectas condiciones","26-11-2014");



DROP TABLE IF EXISTS carreras;

CREATE TABLE `carreras` (
  `id` int(255) NOT NULL auto_increment,
  `id_jefe_carrera` int(255) NOT NULL,
  `nombre` varchar(30) collate utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `horas` varchar(4) collate utf8_spanish2_ci NOT NULL,
  `subproyectos` varchar(3) collate utf8_spanish2_ci NOT NULL,
  `cant_uc` varchar(3) collate utf8_spanish2_ci NOT NULL,
  `fecha_registro` varchar(12) collate utf8_spanish2_ci NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO carreras VALUES("1","2","Ingenieria en Informatica","Carrera de tecnologia dedicada a la rama de la informatica","3904","54","184","26-11-2014");
INSERT INTO carreras VALUES("2","1","T.S.U. en Informática","","1968","35","106","27-11-2014");
INSERT INTO carreras VALUES("3","1","Ingeniería Civil","","3904","54","184","27-11-2014");
INSERT INTO carreras VALUES("4","1","Ingeniería de Petróleo","","3904","54","184","27-11-2014");
INSERT INTO carreras VALUES("5","1","Ingeniería Agrícola","","3904","54","184","27-11-2014");
INSERT INTO carreras VALUES("6","1","T.S.U. en Construcción Civil","","2528","36","105","27-11-2014");



DROP TABLE IF EXISTS horarios;

CREATE TABLE `horarios` (
  `id` int(255) NOT NULL auto_increment,
  `hora` varchar(10) collate utf8_spanish2_ci NOT NULL,
  `id_carrera` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `id_materia` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `id_profesor` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `id_seccion` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `id_aula` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `turno` varchar(2) collate utf8_spanish2_ci NOT NULL,
  `periodo` varchar(15) collate utf8_spanish2_ci NOT NULL,
  `modalidad` varchar(15) collate utf8_spanish2_ci NOT NULL,
  `status` varchar(15) collate utf8_spanish2_ci NOT NULL,
  `fecha_registro` varchar(12) collate utf8_spanish2_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO horarios VALUES("1","","carrera 1","materia 1","profesor 1","Seccion 1","aula 1","N","2014-1","R","Activado","25-11-2014");



DROP TABLE IF EXISTS logs;

CREATE TABLE `logs` (
  `id` int(255) NOT NULL auto_increment,
  `ci` varchar(14) collate utf8_spanish2_ci NOT NULL,
  `accion` varchar(20) collate utf8_spanish2_ci NOT NULL,
  `fecha_registro` varchar(14) collate utf8_spanish2_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;




DROP TABLE IF EXISTS materias;

CREATE TABLE `materias` (
  `id` int(255) NOT NULL auto_increment,
  `id_carrera` int(255) NOT NULL,
  `id_jefe_proyecto` int(255) NOT NULL,
  `codigo` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `nombre` varchar(100) collate utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `horas_trabajo` varchar(2) collate utf8_spanish2_ci NOT NULL,
  `horas_trabajo_practico` varchar(2) collate utf8_spanish2_ci NOT NULL,
  `horas_practico` varchar(2) collate utf8_spanish2_ci NOT NULL,
  `total_horas` varchar(2) collate utf8_spanish2_ci NOT NULL,
  `uc` varchar(3) collate utf8_spanish2_ci NOT NULL,
  `fecha_registro` varchar(12) collate utf8_spanish2_ci NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `codigo` (`codigo`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO materias VALUES("1","2","0","56022101","Cálculo I","","0","0","0","6","4","29-09-2014");
INSERT INTO materias VALUES("2","2","0","56022102","Lógica","","0","0","0","3","2","29-09-2014");
INSERT INTO materias VALUES("3","2","0","56021101","Lenguaje y Comunicación","","0","0","0","4","2","29-09-2014");
INSERT INTO materias VALUES("4","2","0","26023103","Ingles Tecnico I","","0","0","0","4","2","29-09-2014");
INSERT INTO materias VALUES("5","2","0","56023101","Informatica I","","0","0","0","4","3","29-09-2014");
INSERT INTO materias VALUES("6","2","0","56021102","Educacion Fisica","","0","0","0","3","2","29-09-2014");
INSERT INTO materias VALUES("7","2","0","56023102","Organizacion y Sistemas I","","0","0","0","3","3","29-09-2014");
INSERT INTO materias VALUES("8","2","0","56022201","Calculo II","","0","0","0","6","4","29-09-2014");
INSERT INTO materias VALUES("9","2","0","56022202","Estadisticas y Probabilidades","","0","0","0","4","3","29-09-2014");
INSERT INTO materias VALUES("10","2","0","56023201","Informatica II","","0","0","0","4","3","29-09-2014");
INSERT INTO materias VALUES("11","2","0","56023202","Lenguaje y Programacion Basica","","0","0","0","3","2","29-09-2014");
INSERT INTO materias VALUES("12","2","0","56023203","Organizacion y Sistemas II","","0","0","0","3","3","29-09-2014");
INSERT INTO materias VALUES("13","2","0","56022203","Introduccion a la Electricidad","","0","0","0","5","3","29-09-2014");
INSERT INTO materias VALUES("14","2","0","56022301","Algebra","","0","0","0","4","3","29-09-2014");
INSERT INTO materias VALUES("15","2","0","56023305","Ingles Tecnico II","","0","0","0","4","2","29-09-2014");
INSERT INTO materias VALUES("16","2","0","56022302","Contabilidad General","","0","0","0","4","3","29-09-2014");
INSERT INTO materias VALUES("17","2","0","56023301","Sistema de Información I","","0","0","0","3","3","29-09-2014");
INSERT INTO materias VALUES("18","2","0","56023302","Arquitectura del Computador I","","0","0","0","3","3","29-09-2014");
INSERT INTO materias VALUES("19","2","0","56023303","Lenguaje de Programación II Pascal","","0","0","0","3","2","29-09-2014");
INSERT INTO materias VALUES("20","2","0","56023304","Informática III","","0","0","0","4","3","29-09-2014");
INSERT INTO materias VALUES("21","2","0","56023401","Sistema de Información II","","0","0","0","3","3","29-09-2014");
INSERT INTO materias VALUES("22","2","0","56023402","Teleprocesos y Comunicación de Datos I","","0","0","0","4","3","29-09-2014");
INSERT INTO materias VALUES("23","2","0","56023103","Bases de Datos I","","0","0","0","4","3","29-09-2014");
INSERT INTO materias VALUES("24","2","0","56023404","Lenguaje de Programación III Lenguaje C","","0","0","0","3","2","29-09-2014");
INSERT INTO materias VALUES("25","2","0","56023405","Manejo de Microcomputadores","","0","0","0","4","3","29-09-2014");
INSERT INTO materias VALUES("26","2","0","56023406","Paquetes de Aplicación","","0","0","0","6","4","29-09-2014");
INSERT INTO materias VALUES("27","2","0","56023501","Arquitectura del Computador II","","0","0","0","4","3","29-09-2014");
INSERT INTO materias VALUES("28","2","0","56023502","Sistema de Información III","","0","0","0","3","3","29-09-2014");
INSERT INTO materias VALUES("29","2","0","56023503","Teleprocesos y Comunicación de Datos II","","0","0","0","4","3","29-09-2014");
INSERT INTO materias VALUES("30","2","0","56023504","Bases de Datos II","","0","0","0","3","2","29-09-2014");
INSERT INTO materias VALUES("31","2","0","56021501","Técnicas de Investigación Documental","","0","0","0","3","3","29-09-2014");
INSERT INTO materias VALUES("32","2","0","56023506","Informática y Ética","","0","0","0","2","2","29-09-2014");
INSERT INTO materias VALUES("33","2","0","56023505","Lenguaje de Programación IV Cobol","","0","0","0","3","2","29-09-2014");
INSERT INTO materias VALUES("34","2","0","56023601","Pasantia","","0","0","0","0","10","29-09-2014");
INSERT INTO materias VALUES("35","2","0","56023602","Trabajo especial de grado","","0","0","0","0","8","29-09-2014");



DROP TABLE IF EXISTS materias_extendida;

CREATE TABLE `materias_extendida` (
  `id` int(255) NOT NULL auto_increment,
  `id_materia` int(255) NOT NULL,
  `id_profesor` int(255) NOT NULL,
  `id_periodo` int(255) NOT NULL,
  `fecha_registro` varchar(12) collate utf8_spanish2_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;




DROP TABLE IF EXISTS periodo;

CREATE TABLE `periodo` (
  `id` int(255) NOT NULL auto_increment,
  `nombre` varchar(8) collate utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `fecha_registro` varchar(12) collate utf8_spanish2_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO periodo VALUES("1","2014-I","primer periodo del año 2014","26-11-2014");
INSERT INTO periodo VALUES("2","2014-I","primer periodo del año 2014","26-11-2014");



DROP TABLE IF EXISTS secciones;

CREATE TABLE `secciones` (
  `id` int(255) NOT NULL auto_increment,
  `id_carrera` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `nombre` varchar(15) collate utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `turno` varchar(15) collate utf8_spanish2_ci NOT NULL,
  `status` varchar(15) collate utf8_spanish2_ci NOT NULL,
  `fecha_registro` varchar(12) collate utf8_spanish2_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO secciones VALUES("1","","asdasdasd","","Elije la Carrer","Activado","25-11-2014");
INSERT INTO secciones VALUES("2","Elije la Carrera","S01N01","sección 1 nocturno 1","N","Activado","25-11-2014");
INSERT INTO secciones VALUES("3","","Calculo I","calculo i parala ing en informatica","","Activado","26-11-2014");



DROP TABLE IF EXISTS usuarios;

CREATE TABLE `usuarios` (
  `id` int(255) NOT NULL auto_increment,
  `nacionalidad` varchar(1) collate utf8_spanish2_ci NOT NULL,
  `cedula` varchar(12) collate utf8_spanish2_ci NOT NULL,
  `fecha_nac` varchar(12) collate utf8_spanish2_ci NOT NULL,
  `nombres` varchar(15) collate utf8_spanish2_ci NOT NULL,
  `apellidos` varchar(15) collate utf8_spanish2_ci NOT NULL,
  `telefono` varchar(10) collate utf8_spanish2_ci NOT NULL,
  `correo` varchar(150) collate utf8_spanish2_ci NOT NULL,
  `direccion` varchar(150) collate utf8_spanish2_ci NOT NULL,
  `tipo_contrato` varchar(30) collate utf8_spanish2_ci NOT NULL,
  `tipo_user` varchar(20) collate utf8_spanish2_ci NOT NULL,
  `sede` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `id_carrera` int(255) NOT NULL,
  `turno` varchar(10) collate utf8_spanish2_ci NOT NULL,
  `modalidad` varchar(10) collate utf8_spanish2_ci NOT NULL,
  `users` varchar(30) collate utf8_spanish2_ci NOT NULL,
  `clave` varchar(32) collate utf8_spanish2_ci NOT NULL,
  `estatus` varchar(10) collate utf8_spanish2_ci NOT NULL,
  `titulo_pregrado` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `universidad_pregrado` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `ano_pregrado` varchar(4) collate utf8_spanish2_ci NOT NULL,
  `titulo_especializacion` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `universidad_especializacion` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `ano_especializacion` varchar(4) collate utf8_spanish2_ci NOT NULL,
  `titulo_maestria` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `universidad_maestria` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `ano_maestria` varchar(4) collate utf8_spanish2_ci NOT NULL,
  `titulo_doctorado` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `universidad_doctorado` varchar(255) collate utf8_spanish2_ci NOT NULL,
  `ano_doctorado` varchar(4) collate utf8_spanish2_ci NOT NULL,
  `categoria_inicial` varchar(10) collate utf8_spanish2_ci NOT NULL,
  `categoria_actual` varchar(10) collate utf8_spanish2_ci NOT NULL,
  `dedicacion_inicial` varchar(10) collate utf8_spanish2_ci NOT NULL,
  `fecha_ingreso` varchar(12) collate utf8_spanish2_ci NOT NULL,
  `fecha_ultimo_ascenso` varchar(12) collate utf8_spanish2_ci NOT NULL,
  `fecha_proximo_ascenso` varchar(12) collate utf8_spanish2_ci NOT NULL,
  `fecha_probable_jubilacion` varchar(12) collate utf8_spanish2_ci NOT NULL,
  `fecha_registro` varchar(12) collate utf8_spanish2_ci NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `cedula` (`cedula`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

INSERT INTO usuarios VALUES("1","","00.000.000","","mario","aguilar","0000000001","mario@aguilar.com","ninguna direccion establecida ejemlo direccion","completo","admin","","0","todos","N","marloncepeda","4AC131057D42FF759631EC1D60D2A221","Activado","","","","","","","","","","","","","","","","","","","","29-09-2014");
INSERT INTO usuarios VALUES("2","V","00.232.000","20-09-1990","marlon","cepeda","2312312312","warlhunters@gmailss.com","lomas de alto barinas, conjunto agua clara B casa 18","Fijo","users","","0","N","R","","1F973C8146CE088DC1B7C4392A42C87D","Activado","","unellez","","","","","","","","","","","","","","20-06-2014","","","","27-11-2014");
INSERT INTO usuarios VALUES("3","V","00.232.001","20-09-1990","marlon","cepeda","5842457134","yohanna@yohann1a.com","barinas","Contratado","users","","0","D","R","marlon","4AC131057D42FF759631EC1D60D2A221","Activado","","","","","","","","","","","","","","","","","","","","30-11-2014");
INSERT INTO usuarios VALUES("4","V","20.239.293","15-11-1989","yohanna ","graterol","0412153906","yohwanna@yohanna.com","lomas de alto barinas, conjunto agua clara B casa 18","Fijo","opei","","0","D","R","yohannagraterol","4AC131057D42FF759631EC1D60D2A221","Activado","","","","","","","","","","","","","","","","","","","","30-11-2014");



