CREATE DATABASE IF NOT EXISTS buscameen;

USE buscameen;

DROP TABLE IF EXISTS sv_categoria;

CREATE TABLE `sv_categoria` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(35) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `clave` varchar(300) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

INSERT INTO sv_categoria VALUES("1","Academias","Baile, Modelaje, Cursos, Idiomas, Teatro, Danzas.","");
INSERT INTO sv_categoria VALUES("2","Vehiculos","Carros, motos, Lanchas,Accesorios,Camiones, Talleres, Repuestos.","carro");
INSERT INTO sv_categoria VALUES("3","Deportes","Articulos, Accesorios, Club.","");
INSERT INTO sv_categoria VALUES("4","Educacion","Colegios, Liceos, Tecnologicos, Universidades,  Acesorias Tesis.","");
INSERT INTO sv_categoria VALUES("5","Casa - Muebles - Jardín","Venta, Alquiler, Muebles Decoración, Jardín, Accesorios Electrodomésticos Negocios y Oficinas.","");
INSERT INTO sv_categoria VALUES("6","Instituciones","Financieras, Juridicas, Religiosas.","");
INSERT INTO sv_categoria VALUES("7","Recreación","Campamentos","");
INSERT INTO sv_categoria VALUES("8","salud","Publica, Consultorios, Clinicas, Esteticas, spa, Nutricion.","");
INSERT INTO sv_categoria VALUES("9","Tecnologia","Computación, Telefonia,  Laptops, TV , Audio, Video, Cámaras, Accesorios, Video Juegos, Consolas.","");
INSERT INTO sv_categoria VALUES("10","Moda y Belleza","Ropa, Calzado, Relojes, Joyas, Belleza, Bebés y Niños.","");
INSERT INTO sv_categoria VALUES("11","Animales y Mascotas","Perros, Gatos Otros Animales,  Accesorios para Mascotas.","");
INSERT INTO sv_categoria VALUES("12","Fundaciones","Con o sin fines de lucro.","");
INSERT INTO sv_categoria VALUES("13","Servicio","Publicos y privados, Telefonia, Televisión, Transporte, Plomeria, Electricista, Contabilidad, Administración, Informatica.","");
INSERT INTO sv_categoria VALUES("14","Alimentos","Restaurantes, Cafetines, Fruterias, Tiendas Express, Automercados.","");
INSERT INTO sv_categoria VALUES("15","Adultos","Sex Shop","");



DROP TABLE IF EXISTS sv_configuracion;

CREATE TABLE `sv_configuracion` (
  `id` int(255) NOT NULL auto_increment,
  `nombre` varchar(100) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `telefono` varchar(14) NOT NULL,
  ` correo` varchar(150) NOT NULL,
  `facebook` varchar(150) NOT NULL,
  `twitter` varchar(150) NOT NULL,
  `url_logo` varchar(150) NOT NULL,
  `url_fondo` varchar(200) NOT NULL,
  `autor` int(255) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS sv_empresa;

CREATE TABLE `sv_empresa` (
  `id_empresa` int(11) NOT NULL auto_increment,
  `estado` varchar(20) NOT NULL,
  `rif` varchar(15) NOT NULL,
  `nombre_empresa` varchar(65) NOT NULL,
  `id_categoria` varchar(11) NOT NULL,
  `correo` varchar(65) NOT NULL,
  `facebook` varchar(65) NOT NULL,
  `twitter` varchar(65) NOT NULL,
  `instagram` varchar(65) NOT NULL,
  `pagina_web` varchar(65) NOT NULL,
  `telefono` varchar(65) NOT NULL,
  `direccion` varchar(500) NOT NULL,
  `logo` varchar(80) NOT NULL,
  `mapa` varchar(80) NOT NULL,
  `permiso` int(11) NOT NULL,
  `horario` varchar(300) NOT NULL,
  `tipop` varchar(500) NOT NULL,
  `servicio` varchar(1000) NOT NULL,
  `estatus` varchar(15) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha` varchar(12) NOT NULL,
  PRIMARY KEY  (`id_empresa`),
  UNIQUE KEY `rif` (`rif`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

INSERT INTO sv_empresa VALUES("4","Amazonas","19960220","emprsa 2","Anzoategui","warlhunters@gmail.com","http://facebook.com","http://twitter.com","","","584245713467","","","","0","8:00 am - 4:00 pm","Barinas","venta de repuestos","Activado","3","31-10-2014");
INSERT INTO sv_empresa VALUES("6","Barinas","43434343","empresa 5","Apure","warlhunters@gmail.com","http://facebook.com","http://twitter.com","","","584245713467","","","","0","8:00 am - 4:00 pm","Anzoategui","venta de repuestos","Activado","4","31-10-2014");
INSERT INTO sv_empresa VALUES("8","Amazonas","4545","buscameen","Academias","juansanalv@gmail.com","https://www.buscameen.com","https://www.buscame_en","","","2131htt","","","","0","78","Tarjetas de Credito","asdbasnmdb saj djghasj dg dg hsag hdghasg d sa dg ashdghasghdghas gd ghsag hg dhagshdg hasghdgah gdhgash gdhg ash dhgsa d hahsdgshagdh gahg hd ga g gasgdga gdgahg dhg ash sd","Activado","5","14-11-2014");
INSERT INTO sv_empresa VALUES("9","Amazonas","676786","prueba","Academias","juansanalv@hotmail.com","https://www.buscameen.com","https://www.buscame_en","","","56756757","","","","0","78978","Todas","fklsakdhkjhfkj d fshfd shg fhg shd fhgsdhgfhgsahf h","desactivado","5","14-11-2014");
INSERT INTO sv_empresa VALUES("10","Amazonas","89898","ojo","Academias","juansanalv@gmail.comº","https://www.buscameen.com","https://www.buscame_en","","","87897","","","","0","878","Tarjetas de Credito","fhdskhfhjh hjhf hfjhsj hfjhsj fjhjhfd","desactivado","5","14-11-2014");
INSERT INTO sv_empresa VALUES("11","Amazonas","657676","prueba2","Academias","juansanalv@gmail.com","https://www.buscameen.com","https://www.buscame_en","","","8789788778","","","","0","8978","Tarjetas de Debito","Describe Todos tus Servicios jhjk hjkhj","desactivado","5","14-11-2014");
INSERT INTO sv_empresa VALUES("12","Amazonas","kjiuji","prueba3","Academias","juansanalv@gmail.com","https://www.buscameen.com","https://www.buscame_en","","","8678768","","","","0","787","Tarjetas de Credito","hjkhj bh hjkhj h","desactivado","5","14-11-2014");
INSERT INTO sv_empresa VALUES("13","Amazonas","asdasdasd","123123d","Vehiculos","cepdechacademia@gmail.com","http://facebook.com","http://twitter.com","http://instagram.com","http://dominio.com","2232233","asdasdada","","","0","sdasdasdasda","Efectivo","Dasdasdadasdadadasdas","desactivado","3","22-11-2014");
INSERT INTO sv_empresa VALUES("14","Barinas","199929","marloncepeda","Vehiculos","warlhunters@gmail.com","http://facebook.com","http://twitter.com","http://instagram.com","http://dominio.com","584245713467","barinas av agustin figeredo, urb llano alto casa B2P","light-191.png","light-194.png","0","ninguna","Efectivo","Describe Todos tus Servicios","Activado","3","22-11-2014");



DROP TABLE IF EXISTS sv_empresa_palabra;

CREATE TABLE `sv_empresa_palabra` (
  `id` int(255) NOT NULL auto_increment,
  `id_empresa` int(11) NOT NULL,
  `id_palabra` int(11) NOT NULL,
  `palabra` varchar(65) NOT NULL,
  `fecha` varchar(14) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

INSERT INTO sv_empresa_palabra VALUES("15","7","0","celulares","07-11-2014");
INSERT INTO sv_empresa_palabra VALUES("16","7","0","computadoras","07-11-2014");
INSERT INTO sv_empresa_palabra VALUES("17","7","0","computadoras","07-11-2014");
INSERT INTO sv_empresa_palabra VALUES("18","7","0","computadoras","07-11-2014");
INSERT INTO sv_empresa_palabra VALUES("19","7","0","computadoras","07-11-2014");
INSERT INTO sv_empresa_palabra VALUES("21","6","0","bujias","07-11-2014");
INSERT INTO sv_empresa_palabra VALUES("22","6","0","Seleccione una Palabra","07-11-2014");
INSERT INTO sv_empresa_palabra VALUES("23","6","0","computadoras","07-11-2014");
INSERT INTO sv_empresa_palabra VALUES("24","6","0","computadoras","07-11-2014");



DROP TABLE IF EXISTS sv_informacion;

CREATE TABLE `sv_informacion` (
  `id` int(255) NOT NULL auto_increment,
  `num_slider` varchar(1) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `ruta_img` varchar(255) NOT NULL,
  `autor` int(255) NOT NULL,
  `fecha` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS sv_locales;

CREATE TABLE `sv_locales` (
  `id_local` int(11) NOT NULL auto_increment,
  `logo` varchar(65) NOT NULL,
  PRIMARY KEY  (`id_local`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO sv_locales VALUES("1","images/baner01.jpg");
INSERT INTO sv_locales VALUES("2","images/baner02.jpg");
INSERT INTO sv_locales VALUES("3","images/baner03.jpg");



DROP TABLE IF EXISTS sv_logs;

CREATE TABLE `sv_logs` (
  `id` int(255) NOT NULL auto_increment,
  `ci_user` varchar(22) NOT NULL,
  `accion` varchar(35) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=336 DEFAULT CHARSET=utf8;

INSERT INTO sv_logs VALUES("1","","Ver Libros Prestados","28-10-2014");
INSERT INTO sv_logs VALUES("2","","Ver Libros Prestados","28-10-2014");
INSERT INTO sv_logs VALUES("3","","Ver Libros Prestados","28-10-2014");
INSERT INTO sv_logs VALUES("4","","Ver Libros Prestados","28-10-2014");
INSERT INTO sv_logs VALUES("5","","Ver Libros Prestados","28-10-2014");
INSERT INTO sv_logs VALUES("6","","Ver Libros Prestados","28-10-2014");
INSERT INTO sv_logs VALUES("7","","Ver Libros Prestados","28-10-2014");
INSERT INTO sv_logs VALUES("8","","Ver Libros Prestados","28-10-2014");
INSERT INTO sv_logs VALUES("9","","Ver Libros Prestados","28-10-2014");
INSERT INTO sv_logs VALUES("10","","Ver Libros Prestados","28-10-2014");
INSERT INTO sv_logs VALUES("11","","Ver Libros Prestados","28-10-2014");
INSERT INTO sv_logs VALUES("12","","Ver Perfil","28-10-2014");
INSERT INTO sv_logs VALUES("13","","Ver Libros Prestados","28-10-2014");
INSERT INTO sv_logs VALUES("14","","Ver Libros Prestados","28-10-2014");
INSERT INTO sv_logs VALUES("15","","Ver Libros Prestados","28-10-2014");
INSERT INTO sv_logs VALUES("16","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("17","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("18","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("19","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("20","","Salir","29-10-2014");
INSERT INTO sv_logs VALUES("21","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("22","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("23","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("24","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("25","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("26","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("27","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("28","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("29","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("30","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("31","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("32","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("33","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("34","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("35","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("36","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("37","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("38","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("39","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("40","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("41","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("42","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("43","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("44","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("45","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("46","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("47","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("48","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("49","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("50","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("51","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("52","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("53","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("54","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("55","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("56","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("57","","Salir","29-10-2014");
INSERT INTO sv_logs VALUES("58","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("59","","Salir","29-10-2014");
INSERT INTO sv_logs VALUES("60","","Ver Libros Prestados","29-10-2014");
INSERT INTO sv_logs VALUES("61","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("62","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("63","","Salir","31-10-2014");
INSERT INTO sv_logs VALUES("64","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("65","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("66","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("67","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("68","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("69","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("70","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("71","","Salir","31-10-2014");
INSERT INTO sv_logs VALUES("72","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("73","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("74","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("75","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("76","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("77","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("78","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("79","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("80","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("81","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("82","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("83","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("84","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("85","","Bloquear Usuario","31-10-2014");
INSERT INTO sv_logs VALUES("86","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("87","","Salir","31-10-2014");
INSERT INTO sv_logs VALUES("88","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("89","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("90","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("91","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("92","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("93","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("94","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("95","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("96","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("97","","Salir","31-10-2014");
INSERT INTO sv_logs VALUES("98","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("99","","Salir","31-10-2014");
INSERT INTO sv_logs VALUES("100","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("101","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("102","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("103","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("104","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("105","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("106","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("107","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("108","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("109","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("110","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("111","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("112","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("113","","Salir","31-10-2014");
INSERT INTO sv_logs VALUES("114","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("115","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("116","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("117","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("118","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("119","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("120","","Salir","31-10-2014");
INSERT INTO sv_logs VALUES("121","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("122","","Ver Libros Prestados","31-10-2014");
INSERT INTO sv_logs VALUES("123","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("124","","Bloquear Usuario","31-10-2014");
INSERT INTO sv_logs VALUES("125","","Ver Listas de Usuarios","31-10-2014");
INSERT INTO sv_logs VALUES("126","","Salir","31-10-2014");
INSERT INTO sv_logs VALUES("127","","Ver Libros Prestados","01-11-2014");
INSERT INTO sv_logs VALUES("128","","Ver Libros Prestados","01-11-2014");
INSERT INTO sv_logs VALUES("129","","Ver Libros Prestados","06-11-2014");
INSERT INTO sv_logs VALUES("130","","Ver Listas de Usuarios","06-11-2014");
INSERT INTO sv_logs VALUES("131","","Salir","06-11-2014");
INSERT INTO sv_logs VALUES("132","","Ver Libros Prestados","06-11-2014");
INSERT INTO sv_logs VALUES("133","","Ver Listas de Usuarios","06-11-2014");
INSERT INTO sv_logs VALUES("134","","Ver Listas de Usuarios","06-11-2014");
INSERT INTO sv_logs VALUES("135","","Ver Listas de Usuarios","06-11-2014");
INSERT INTO sv_logs VALUES("136","","Ver Listas de Usuarios","06-11-2014");
INSERT INTO sv_logs VALUES("137","","Ver Listas de Usuarios","06-11-2014");
INSERT INTO sv_logs VALUES("138","","Ver Listas de Usuarios","06-11-2014");
INSERT INTO sv_logs VALUES("139","","Ver Listas de Usuarios","06-11-2014");
INSERT INTO sv_logs VALUES("140","","Ver Listas de Usuarios","06-11-2014");
INSERT INTO sv_logs VALUES("141","","Ver Listas de Usuarios","06-11-2014");
INSERT INTO sv_logs VALUES("142","","Ver Listas de Usuarios","06-11-2014");
INSERT INTO sv_logs VALUES("143","","Ver Listas de Usuarios","06-11-2014");
INSERT INTO sv_logs VALUES("144","","Ver Listas de Usuarios","06-11-2014");
INSERT INTO sv_logs VALUES("145","","Salir","06-11-2014");
INSERT INTO sv_logs VALUES("146","","Ver Libros Prestados","07-11-2014");
INSERT INTO sv_logs VALUES("147","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("148","","Desbloquear Usuario","07-11-2014");
INSERT INTO sv_logs VALUES("149","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("150","","Salir","07-11-2014");
INSERT INTO sv_logs VALUES("151","","Ver Libros Prestados","07-11-2014");
INSERT INTO sv_logs VALUES("152","","Ver Libros Prestados","07-11-2014");
INSERT INTO sv_logs VALUES("153","","Salir","07-11-2014");
INSERT INTO sv_logs VALUES("154","","Ver Libros Prestados","07-11-2014");
INSERT INTO sv_logs VALUES("155","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("156","","Ver Libros Prestados","07-11-2014");
INSERT INTO sv_logs VALUES("157","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("158","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("159","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("160","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("161","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("162","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("163","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("164","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("165","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("166","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("167","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("168","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("169","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("170","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("171","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("172","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("173","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("174","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("175","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("176","","Ver Libros Prestados","07-11-2014");
INSERT INTO sv_logs VALUES("177","","Ver Libros Prestados","07-11-2014");
INSERT INTO sv_logs VALUES("178","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("179","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("180","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("181","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("182","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("183","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("184","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("185","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("186","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("187","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("188","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("189","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("190","","Ver Listas de Usuarios","07-11-2014");
INSERT INTO sv_logs VALUES("191","","Salir","07-11-2014");
INSERT INTO sv_logs VALUES("192","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("193","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("194","","Salir","14-11-2014");
INSERT INTO sv_logs VALUES("195","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("196","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("197","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("198","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("199","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("200","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("201","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("202","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("203","","Salir","14-11-2014");
INSERT INTO sv_logs VALUES("204","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("205","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("206","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("207","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("208","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("209","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("210","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("211","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("212","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("213","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("214","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("215","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("216","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("217","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("218","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("219","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("220","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("221","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("222","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("223","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("224","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("225","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("226","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("227","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("228","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("229","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("230","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("231","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("232","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("233","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("234","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("235","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("236","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("237","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("238","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("239","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("240","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("241","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("242","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("243","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("244","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("245","","Salir","14-11-2014");
INSERT INTO sv_logs VALUES("246","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("247","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("248","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("249","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("250","","Salir","14-11-2014");
INSERT INTO sv_logs VALUES("251","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("252","","Salir","14-11-2014");
INSERT INTO sv_logs VALUES("253","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("254","","Salir","14-11-2014");
INSERT INTO sv_logs VALUES("255","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("256","","Ver Listas de Usuarios","14-11-2014");
INSERT INTO sv_logs VALUES("257","","Salir","14-11-2014");
INSERT INTO sv_logs VALUES("258","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("259","","Salir","14-11-2014");
INSERT INTO sv_logs VALUES("260","","Ver Libros Prestados","14-11-2014");
INSERT INTO sv_logs VALUES("261","","Ver Libros Prestados","21-11-2014");
INSERT INTO sv_logs VALUES("262","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("263","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("264","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("265","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("266","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("267","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("268","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("269","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("270","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("271","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("272","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("273","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("274","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("275","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("276","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("277","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("278","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("279","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("280","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("281","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("282","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("283","","Salir","22-11-2014");
INSERT INTO sv_logs VALUES("284","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("285","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("286","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("287","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("288","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("289","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("290","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("291","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("292","","Salir","22-11-2014");
INSERT INTO sv_logs VALUES("293","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("294","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("295","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("296","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("297","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("298","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("299","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("300","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("301","","Salir","22-11-2014");
INSERT INTO sv_logs VALUES("302","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("303","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("304","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("305","","Salir","22-11-2014");
INSERT INTO sv_logs VALUES("306","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("307","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("308","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("309","","Salir","22-11-2014");
INSERT INTO sv_logs VALUES("310","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("311","","Salir","22-11-2014");
INSERT INTO sv_logs VALUES("312","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("313","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("314","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("315","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("316","","Salir","22-11-2014");
INSERT INTO sv_logs VALUES("317","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("318","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("319","","Salir","22-11-2014");
INSERT INTO sv_logs VALUES("320","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("321","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("322","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("323","","Salir","22-11-2014");
INSERT INTO sv_logs VALUES("324","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("325","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("326","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("327","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("328","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("329","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("330","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("331","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("332","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("333","","Ver Listas de Usuarios","22-11-2014");
INSERT INTO sv_logs VALUES("334","","Ver Libros Prestados","22-11-2014");
INSERT INTO sv_logs VALUES("335","","Salir","22-11-2014");



DROP TABLE IF EXISTS sv_mensajes_internos;

CREATE TABLE `sv_mensajes_internos` (
  `id` int(255) NOT NULL auto_increment,
  `nombres` varchar(30) collate utf8_bin NOT NULL,
  `email` varchar(150) collate utf8_bin NOT NULL,
  `telefono` varchar(14) collate utf8_bin NOT NULL,
  `mensaje` text collate utf8_bin NOT NULL,
  `estado` varchar(15) collate utf8_bin NOT NULL,
  `fecha` varchar(12) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO sv_mensajes_internos VALUES("1","stivens vilera","warlhunters@gmail.com","584245713467","asdasdasdas","Barinas","31-10-2014");
INSERT INTO sv_mensajes_internos VALUES("2","stivens vilera","warlhunters@gmail.com","584245713467","asdasdasdas","Barinas","31-10-2014");
INSERT INTO sv_mensajes_internos VALUES("3","stivens vilera","warlhunters@gmail.com","584245713467","asdasdasdas","Barinas","31-10-2014");
INSERT INTO sv_mensajes_internos VALUES("4","stivens vilera","warl_13@hotmail.com","584245713467","asdasdasdasdasdas","Barinas","31-10-2014");
INSERT INTO sv_mensajes_internos VALUES("5","juan","juansanalv@gmail.com","78787","hjh hj hj hjh jhjk hhjhjkh","Amazonas","14-11-2014");
INSERT INTO sv_mensajes_internos VALUES("6","juan","juansanalv@gmail.com","78787","hjh hj hj hjh jhjk hhjhjkh","Amazonas","14-11-2014");



DROP TABLE IF EXISTS sv_palabras;

CREATE TABLE `sv_palabras` (
  `id` int(11) NOT NULL auto_increment,
  `palabra` varchar(65) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO sv_palabras VALUES("1","auto");
INSERT INTO sv_palabras VALUES("2","bujias");
INSERT INTO sv_palabras VALUES("3","repuestos");
INSERT INTO sv_palabras VALUES("4","celulares");
INSERT INTO sv_palabras VALUES("5","telefono");
INSERT INTO sv_palabras VALUES("6","computadoras");
INSERT INTO sv_palabras VALUES("7","cauchos");
INSERT INTO sv_palabras VALUES("8","balones");
INSERT INTO sv_palabras VALUES("9","papeleria");
INSERT INTO sv_palabras VALUES("10","piñateria");



DROP TABLE IF EXISTS sv_personal;

CREATE TABLE `sv_personal` (
  `id` int(11) NOT NULL auto_increment,
  `per_nombre` varchar(150) character set utf8 collate utf8_bin NOT NULL,
  `per_apellido` varchar(100) character set utf8 collate utf8_bin NOT NULL default '',
  `per_direccion` varchar(500) character set utf8 collate utf8_bin default NULL,
  `per_direccion1` varchar(30) NOT NULL,
  `per_direccion2` varchar(150) character set utf8 collate utf8_bin NOT NULL,
  `per_direccion3` varchar(150) character set utf8 collate utf8_bin NOT NULL,
  `per_telefono` varchar(50) character set utf8 collate utf8_bin NOT NULL,
  `per_telefono1` varchar(50) character set utf8 collate utf8_bin NOT NULL,
  `per_correo` varchar(80) character set utf8 collate utf8_bin NOT NULL,
  `per_facebook` varchar(300) character set utf8 collate utf8_bin NOT NULL,
  `per_twitter` varchar(300) character set utf8 collate utf8_bin NOT NULL,
  `per_google` varchar(300) character set utf8 collate utf8_bin NOT NULL,
  `usu_cedula` varchar(20) character set utf8 collate utf8_bin NOT NULL,
  `usu_codigo` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `usu_cedula` (`usu_cedula`,`usu_codigo`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO sv_personal VALUES("6","gabriel","rojas","aqui","5","Barrio la Paz","casa 113","0273-5350116","0412-4289536","karpofv.89@hotmail.com","http://facebook.com","http://twiter.com","","19191493","4");
INSERT INTO sv_personal VALUES("7","Cesar","Garcia","Calle","9","alto barinas","casa 113","0273-5350116","0412-4289535","karpofv.89@hotmail.com","http://facebook.com","http://twiter.com","","19517079","5");



DROP TABLE IF EXISTS sv_users;

CREATE TABLE `sv_users` (
  `id` int(255) NOT NULL auto_increment,
  `nacionalidad` varchar(1) NOT NULL,
  `ci` varchar(27) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `apellido` varchar(15) NOT NULL,
  `usuario` varchar(15) NOT NULL,
  `clave` varchar(60) NOT NULL,
  `fecha_nac` varchar(10) NOT NULL,
  `tipo_user` varchar(15) NOT NULL,
  `estado` varchar(10) NOT NULL,
  `correo` varchar(150) NOT NULL,
  `telefono` varchar(12) NOT NULL,
  `ruta_img` varchar(200) NOT NULL,
  `pregunta` varchar(30) NOT NULL,
  `respuesta` varchar(30) NOT NULL,
  `fecha_registro` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `usuario` (`usuario`),
  UNIQUE KEY `correo` (`correo`),
  UNIQUE KEY `ci` (`ci`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO sv_users VALUES("4","","20239293","yohanna ","graterol","yohanna","4AC131057D42FF759631EC1D60D2A221","","users","activo","yohannag15@hotmail.com","","","","","31-10-2014");
INSERT INTO sv_users VALUES("2","","00.000.013","stivens","vilera","admin","4AC131057D42FF759631EC1D60D2A221","","users","bloqueado","warlhunters@gmail.com","","","","","28-10-2014");
INSERT INTO sv_users VALUES("3","","19.960.220","marlon stivens","cepeda vilera","marloncepeda","4AC131057D42FF759631EC1D60D2A221","","admin","activo","warl_13@hotmail.com","","","","","28-10-2014");
INSERT INTO sv_users VALUES("5","","123456","juan","sanchez","juansanalv","C9EE71485F2EC92877E91731815AA3EB","","users","activo","juansanalv@gmail.com","","","","","14-11-2014");



