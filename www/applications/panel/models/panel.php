<?php
/**
 * Access from index.php:
 */
if(!defined("_access")) {
	die("Error: You don't have permission to access here...");
}

class panel_Model extends ZP_Model {
	
	public function __construct() {
		$this->Db = $this->db();		
		$this->Data = $this->core("Data");
		$this->helpers();
		$this->table_users = "usuarios";
	}
	public function registrar($clave){
		 $data = array(
		 	"cedula"=>POST("cedula"),
		 	"nacionalidad"=>POST("nacionalidad"),
		 	"fecha_nac"=>POST("fecha_nac"),
		 	"nombres"=>POST("nombres"),
		 	"apellidos"=>POST("apellidos"),
		 	"telefono"=>POST("telefono"),
		 	"correo"=>POST("correo"),
		 	"direccion"=>POST("direccion"),
		 	"tipo_contrato"=>POST("tipo_contrato"),
		 	"tipo_user"=>"users",
		 	"estatus"=>"Activado",
		 	"turno"=>POST("turno"),
		 	"modalidad"=>POST("modalidad"),
		 	"users"=>POST("usuario"),
		 	"clave"=>$clave,
		 	"titulo_pregrado"=>POST("titulo_pregrado"),
		 	"universidad_pregrado"=>POST("universidad_pregrado"),
		 	"ano_pregrado"=>POST("ano_pregrado"),
		 	"titulo_especializacion"=>POST("titulo_especializacion"),
		 	"universidad_especializacion"=>POST("universidad_especializacion"),
		 	"ano_especializacion"=>POST("ano_especializacion"),
		 	"titulo_maestria"=>POST("titulo_maestria"),
		 	"universidad_maestria"=>POST("universidad_maestria"),
		 	"ano_maestria"=>POST("ano_maestria"),
		 	"titulo_doctorado"=>POST("titulo_doctorado"),
		 	"universidad_doctorado"=>POST("universidad_doctorado"),
		 	"ano_doctorado"=>POST("ano_doctorado"),
		 	"categoria_inicial"=>POST("categoria_inicial"),
		 	"categoria_actual"=>POST("categoria_actual"),
		 	"dedicacion_inicial"=>POST("dedicacion_inicial"),
		 	"fecha_ingreso"=>POST("fecha_ingreso"),
		 	"fecha_ultimo_ascenso"=>POST("fecha_ultimo_ascenso"),
		 	"fecha_proximo_ascenso"=>POST("fecha_proximo_ascenso"),
		 	"fecha_probable_jubilacion"=>POST("fecha_probable_jubilacion"),
			"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("usuarios", $data);
	}
	public function login($clave){
		$username = POST("usuario");
		$this->Db->table($this->table_users);
		return $data = $this->Db->findBySQL("users='$username' AND clave='$clave'");
	}
	
	public function modificar_perfil($users_id,$namefile){
		$data = array(
		 	"ci"=>POST("ci"),
		 	"ruta_img"=>$namefile,
		 	"nombre"=>POST("nombres"),
		 	"apellido"=>POST("apellidos"),
		 	"telefono"=>POST("telefono"),
		 	"fecha_nac"=>POST("fecha"),
			"correo"=>POST("email")
		);
		$this->Db->update("users", $data, $users_id);
	}
	public function modificar_clave($user_id){
		$data = array(
		 	"clave"=>POST("clave")
		);
		$this->Db->update("users", $data, $user_id);
	}
	public function buscar_pregunta_usuario($correo){
		$this->Db->table($this->table_users);
		return $data = $this->Db->findBySQL("correo='$correo'");
	}
	public function usuario_borrar($id){
		 $this->Db->delete($id, $this->table_users);
	}
	public function usuario_estado($id,$status){
		$data = array(
		 	"estatus"=>$status
		);
		$this->Db->update("usuarios", $data, $id);
	}
	public function buscar_todos_usuarios(){
		return $todos_usuarios = $this->Db->findAll($this->table_users);
	}
	public function logs($accion){
		 $data = array(
		 	"ci_user"=>SESSION("ci"),
		 	"accion"=>$accion,
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("logs", $data);
	}

	public function buscar_logs($ci_user){
		$this->Db->table($this->table_logs);
		return $data = $this->Db->findBySQL("ci_user='$ci_user' ORDER BY id  DESC LIMIT 10 ");
	}
	public function horario_agregar(){
		$data = array(
		 	"hora"=>POST("hora"),
		 	"id_carrera"=>POST("id_carrera"),
		 	"id_materia"=>POST("id_materia"),
		 	"id_profesor"=>POST("id_profesor"),
		 	"id_seccion"=>POST("id_seccion"),
		 	"id_aula"=>POST("id_aula"),
		 	"turno"=>POST("turno"),
		 	"periodo"=>POST("periodo"),
		 	"modalidad"=>POST("modalidad"),
		 	"status"=>"Activado",
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("horarios", $data);
	}
	public function buscar_todos_horarios(){
		return $data = $this->Db->findAll("horarios");
	}
	public function seccion_agregar(){
		 $data = array(
		 	"id_carrera"=>POST("id_carrera"),
		 	"nombre"=>POST("nombre"),
		 	"descripcion"=>POST("descripcion"),
		 	"turno"=>POST("turno"),
		 	"status"=>"Activado",
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("secciones", $data);
	}
	public function buscar_todos_secciones(){
		return $data = $this->Db->findAll("secciones");
	}
	public function aula_agregar(){
		 $data = array(
		 	"numero_pabellon"=>POST("numero_pabellon"),
		 	"numero_salon"=>POST("numero_salon"),
		 	"descripcion"=>POST("descripcion"),
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("aulas", $data);
	}
	public function buscar_todos_aulas(){
		return $data = $this->Db->findAll("aulas");
	}
	public function carrera_agregar(){
		 $data = array(
		 	"id_jefe_carrera"=>POST("id_jefe_proyecto"),
		 	"nombre"=>POST("nombre"),
		 	"descripcion"=>POST("descripcion"),
		 	"horas"=>POST("total_horas"),
		 	"subproyectos"=>POST("num_materias"),
		 	"cant_uc"=>POST("uc"),
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("carreras", $data);
	}
	public function buscar_todos_carreras(){
		return $data = $this->Db->findAll("carreras");
	}
	public function materia_agregar(){
		 $data = array(
		 	"id_carrera"=>POST("id_carrera"),
		 	"id_jefe_proyecto"=>POST("id_jefe_proyecto"),
		 	"codigo"=>POST("codigo"),
		 	"nombre"=>POST("nombre"),
		 	"descripcion"=>POST("descripcion"),
		 	"horas_trabajo"=>POST("ht"),
		 	"horas_trabajo_practico"=>POST("htp"),
		 	"horas_practico"=>POST("hp"),
		 	"total_horas"=>POST("total_horas"),
		 	"uc"=>POST("uc"),
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("materias", $data);
	}
	public function materia_asignar(){
		$data = array(
		 	"id_materia"=>POST("id_materia"),
		 	"id_profesor"=>POST("id_profesor"),
		 	"id_periodo"=>POST("id_periodo"),
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("materias_extendida", $data);
	}
	public function buscar_todos_materias(){
		return $data = $this->Db->findAll("materias");
	}
	public function periodo_agregar(){
		 $data = array(
		 	"nombre"=>POST("nombre"),
		 	"descripcion"=>POST("descripcion"),
		 	"fecha_registro"=>date("d-m-Y")
		);
		$this->Db->insert("periodo", $data);
	}
	public function buscar_todos_periodos(){
		return $data = $this->Db->findAll("periodo");
	}
}
